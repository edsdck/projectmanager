﻿namespace BusinessLogic.DataTransferObjects.Assignment
{
    public class AssignmentStatusDto
    {
        public string Id { get; set; }

        public bool Completed { get; set; }
    }
}
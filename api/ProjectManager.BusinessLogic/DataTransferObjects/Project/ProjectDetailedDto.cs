﻿using System.Collections.Generic;

namespace BusinessLogic.DataTransferObjects.Project
{
    public class ProjectDetailedDto
    {
        public string Id { get; set; }
        
        public string Name { get; set; }
        
        public Member Owner { get; set; }
        
        public IEnumerable<Member> Members { get; set; }
        
        public IEnumerable<ProjectAssignment> Assignments { get; set; } 
    }

    public class Member
    {
        public string Name { get; set; }
        
        public string Id { get; set; }
    }

    public class ProjectAssignment
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }
        
        public bool Completed { get; set; }
    }
}
﻿using System.ComponentModel.DataAnnotations;

namespace BusinessLogic.DataTransferObjects.Project
{
    public class AddMemberDto
    {
        [Required]
        public string ProjectId { get; set; }

        [Required]
        public string UserId { get; set; }
    }
}
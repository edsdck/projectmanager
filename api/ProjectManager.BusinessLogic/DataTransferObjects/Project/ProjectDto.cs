﻿namespace BusinessLogic.DataTransferObjects.Project
{
    public class ProjectDto
    {
        public string Id { get; set; }
        
        public string Name { get; set; }

        public int MembersTotal { get; set; }
    }
}
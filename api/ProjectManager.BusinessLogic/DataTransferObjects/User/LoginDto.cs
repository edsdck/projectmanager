﻿namespace BusinessLogic.DataTransferObjects.User
{
    public class LoginDto
    {
        public string LoginName { get; set; }

        public string Password { get; set; }
    }
}
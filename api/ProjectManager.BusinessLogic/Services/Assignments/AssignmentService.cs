﻿using System;
using System.Linq;
using System.Threading.Tasks;
using BusinessLogic.DataTransferObjects.Assignment;
using Microsoft.EntityFrameworkCore;
using ProjectManager.DataLayer;
using ProjectManager.DataLayer.Models;

namespace BusinessLogic.Services.Assignments
{
    public class AssignmentService : IAssignmentService
    {
        private readonly ProjectManagerContext _context;
        private readonly DbSet<Assignment> _assignments;
        
        public AssignmentService(ProjectManagerContext context)
        {
            _context = context;
            _assignments = _context.Set<Assignment>();
        }

        public async Task<Assignment> Create(Assignment assignment)
        {
            assignment.Id = Guid.NewGuid().ToString();
            
            _assignments.Add(assignment);
            await _context.SaveChangesAsync();

            return assignment;
        }
        
        public async Task<Assignment> UpdateAssignmentStatus(AssignmentStatusDto assignmentStatusDto)
        {
            var assignment = await _assignments.FirstOrDefaultAsync(x => x.Id == assignmentStatusDto.Id);
            if (assignment is null)
            {
                return new Assignment();
            }

            assignment.Completed = assignmentStatusDto.Completed;
            await _context.SaveChangesAsync();

            return assignment;
        }
    }
}
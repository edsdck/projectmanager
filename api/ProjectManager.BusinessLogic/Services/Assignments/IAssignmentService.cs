﻿using System.Threading.Tasks;
using BusinessLogic.DataTransferObjects.Assignment;
using ProjectManager.DataLayer.Models;

namespace BusinessLogic.Services.Assignments
{
    public interface IAssignmentService
    {
        Task<Assignment> Create(Assignment assignment);
        
        Task<Assignment> UpdateAssignmentStatus(AssignmentStatusDto assignmentStatusDto);
    }
}
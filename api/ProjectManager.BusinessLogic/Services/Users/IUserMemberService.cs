﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ProjectManager.DataLayer.Models;

namespace BusinessLogic.Services.Users
{
    public interface IUserMemberService
    {
        Task<IEnumerable<User>> GetPotentialMembers(string projectId);
    }
}
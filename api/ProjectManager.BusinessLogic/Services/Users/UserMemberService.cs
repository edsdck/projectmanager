﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ProjectManager.DataLayer;
using ProjectManager.DataLayer.Models;

namespace BusinessLogic.Services.Users
{
    public class UserMemberService : IUserMemberService
    {
        private readonly DbSet<User> _userDb;
        private readonly DbSet<Project> _projectDb;

        public UserMemberService(ProjectManagerContext context)
        {
            _userDb = context.Set<User>();
            _projectDb = context.Set<Project>();
        }

        public async Task<IEnumerable<User>> GetPotentialMembers(string projectId)
        {
            var project = await _projectDb
                .Include(x => x.Members)
                .FirstOrDefaultAsync(x => x.Id == projectId);
            
            if (project is null)
            {
                return new List<User>();
            }
            
            var currentProjectMembers = project.Members.Select(x => x.UserId);

            return await _userDb.Where(x => !currentProjectMembers.Contains(x.Id)).ToListAsync();
        }
    }
}
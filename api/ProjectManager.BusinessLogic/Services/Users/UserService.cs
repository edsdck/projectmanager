﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ProjectManager.DataLayer;
using ProjectManager.DataLayer.Models;

namespace BusinessLogic.Services.Users
{
    public class UserService : IUserService
    {
        private readonly ProjectManagerContext _context;
        private readonly DbSet<User> _userDb;

        public UserService(ProjectManagerContext context)
        {
            _context = context;
            _userDb = _context.Set<User>();
        }

        public async Task<User> Get(string id)
        {
            return await _userDb.FindAsync(id);
        }

        public async Task<IEnumerable<User>> GetAll()
        {
            return await _userDb.ToListAsync();
        }

        public async Task<User> GetByName(string name)
        {
            return await _userDb.FirstOrDefaultAsync(x => x.AdditionalName == name);
        }

        public async Task Add(User user)
        {
            user.Id = Guid.NewGuid().ToString();
            user.FullName = user.FullName.Trim();
            
            _userDb.Add(user);
            await _context.SaveChangesAsync();
        }

        public async Task Update(User user)
        {
            _userDb.Update(user);
            await _context.SaveChangesAsync();
        }

        public async Task<bool> Delete(string id)
        {
            var user = await _userDb.FindAsync(id);
            
            if (user is null)
            {
                return false;
            }
            
            _userDb.Remove(user);
            await _context.SaveChangesAsync();

            return true;
        }
    }
}
﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ProjectManager.DataLayer.Models;

namespace BusinessLogic.Services.Users
{
    public interface IUserService
    {
        Task<User> Get(string id);

        Task<IEnumerable<User>> GetAll();

        Task<User> GetByName(string name);

        Task Add(User user);

        Task Update(User user);

        Task<bool> Delete(string id);
    }
}
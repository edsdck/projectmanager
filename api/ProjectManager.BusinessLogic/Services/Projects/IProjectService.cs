﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BusinessLogic.DataTransferObjects.Project;
using ProjectManager.DataLayer.Models;

namespace BusinessLogic.Services.Projects
{
    public interface IProjectService
    {
        Task<ProjectDto> Get(string id);

        Task<ProjectDetailedDto> GetDetailed(string id);

        Task<IEnumerable<ProjectDto>> GetAll(string userId);
        
        Task Add(Project project);

        Task Update(Project project);

        Task<bool> Delete(string id);

        Task AddMemberToProject(AddMemberDto memberDto);
    }
}
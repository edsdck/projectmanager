﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using BusinessLogic.DataTransferObjects.Project;
using Microsoft.EntityFrameworkCore;
using ProjectManager.DataLayer;
using ProjectManager.DataLayer.Models;

namespace BusinessLogic.Services.Projects
{
    public class ProjectService : IProjectService
    {
        private readonly ProjectManagerContext _context;
        private readonly DbSet<User> _userDb;
        private readonly DbSet<Project> _projectDb;
        private readonly DbSet<UserProject> _userProjectDb;

        public ProjectService(ProjectManagerContext context)
        {
            _context = context;
            _projectDb = _context.Set<Project>();
            _userProjectDb = _context.Set<UserProject>();
            _userDb = _context.Set<User>();
        }
        
        public async Task<ProjectDto> Get(string id)
        {
            return await _projectDb
                .Where(x => x.Id == id)
                .Select(MapProjectDto)
                .FirstOrDefaultAsync();
        }

        public async Task<ProjectDetailedDto> GetDetailed(string id)
        {
            return await _projectDb
                .Where(x => x.Id == id)
                .Select(x => new ProjectDetailedDto
                {
                    Id = x.Id,
                    Name = x.Name,
                    Owner = new Member
                    {
                        Id = x.Owner.Id,
                        Name = x.Owner.FullName
                    },
                    Members = x.Members.Select(m => new Member
                    {
                        Id = m.UserId,
                        Name = m.User.FullName
                    }),
                    Assignments = x.Assignments.Select(a => new ProjectAssignment
                    {
                        Id = a.Id,
                        Name = a.Name,
                        Description = a.Description,
                        Completed = a.Completed
                    })
                })
                .FirstOrDefaultAsync();
        }
        
        public async Task<IEnumerable<ProjectDto>> GetAll(string userId)
        {
            var userProjectIds = await _userProjectDb
                .Where(x => x.UserId == userId)
                .Select(x => x.ProjectId)
                .ToListAsync();
            
            return await _projectDb
                .Where(x => userProjectIds.Contains(x.Id))
                .Select(MapProjectDto)
                .ToListAsync();
        }

        public async Task Add(Project project)
        {
            project.Id = Guid.NewGuid().ToString();
            var userProject = JoinUserAndProject(project.OwnerId, project.Id);
            
            _userProjectDb.Add(userProject);
            _projectDb.Add(project);

            await _context.SaveChangesAsync();
        }

        public async Task Update(Project project)
        {
            _projectDb.Update(project);
            await _context.SaveChangesAsync();
        }

        public async Task<bool> Delete(string id)
        {
            var project = await _projectDb.FindAsync(id);
            
            if (project is null)
            {
                return false;
            }

            var userProject = await _userProjectDb.FindAsync(project.OwnerId, project.Id);
            
            _userProjectDb.Remove(userProject);
            _projectDb.Remove(project);
            await _context.SaveChangesAsync();

            return true;
        }

        public async Task AddMemberToProject(AddMemberDto memberDto)
        {
            if (IsAlreadyMember(memberDto))
            {
                return;
            }
            
            var project = await _projectDb.FindAsync(memberDto.ProjectId);
            var user = await _userDb.FindAsync(memberDto.UserId);
            
            if (project is null ||
                user is null)
            {
                return;
            }

            var userProject = JoinUserAndProject(memberDto.UserId, memberDto.ProjectId);
            _userProjectDb.Add(userProject);

            await _context.SaveChangesAsync();
        }
        
        private static UserProject JoinUserAndProject(string userId, string projectId) =>
            new UserProject
            {
                UserId = userId,
                ProjectId = projectId
            };

        private static Expression<Func<Project, ProjectDto>> MapProjectDto =>
            x => new ProjectDto
            {
                Id = x.Id,
                Name = x.Name,
                MembersTotal = x.Members.Count
            };

        private bool IsAlreadyMember(AddMemberDto memberDto)
        {
            return _userProjectDb.Any(x => x.UserId == memberDto.UserId && 
                                    x.ProjectId == memberDto.ProjectId);
        }
    }
}
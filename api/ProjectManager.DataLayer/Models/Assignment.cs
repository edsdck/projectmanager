﻿using System.ComponentModel.DataAnnotations;

namespace ProjectManager.DataLayer.Models
{
    public class Assignment
    {
        public string Id { get; set; }
        
        [Required]
        public string Name { get; set; }
        
        public string Description { get; set; }
        
        public bool Completed { get; set; }
        
        [Required]
        public string ProjectId { get; set; }
        
        public virtual Project Project { get; set; }
    }
}
﻿
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ProjectManager.DataLayer.Models
{
    public class Project
    {
        public string Id { get; set; }
        
        [Required]
        public string Name { get; set; }
        
        [Required]
        public string OwnerId { get; set; }
        
        public virtual User Owner { get; set; }
        
        public virtual ICollection<Assignment> Assignments { get; set; }
        
        public virtual ICollection<UserProject> Members { get; set; }
    }
}
﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ProjectManager.DataLayer.Models
{
    public class User
    {
        public string Id { get; set; }
        
        [Required]
        public string FullName { get; set; }
        
        public string AdditionalName { get; set; }
        
        [Required]
        public string Password { get; set; }
        
        public string PhoneNumber { get; set; }
        
        public virtual ICollection<UserProject> Projects { get; set; }
    }
}
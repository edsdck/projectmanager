﻿using Microsoft.EntityFrameworkCore;
using ProjectManager.DataLayer.Models;

namespace ProjectManager.DataLayer
{
    public class ProjectManagerContext : DbContext
    {
        public ProjectManagerContext (DbContextOptions options)
            : base(options)
        {
        }
        
        public DbSet<User> Users { get; set; }
        
        public DbSet<Project> Projects { get; set; }
        
        public DbSet<Assignment> Assignments { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UserProject>()
                .HasKey(pt => new { pt.UserId, pt.ProjectId });

            modelBuilder.Entity<UserProject>()
                .HasOne(pt => pt.User)
                .WithMany(p => p.Projects)
                .HasForeignKey(pt => pt.UserId)
                .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<UserProject>()
                .HasOne(pt => pt.Project)
                .WithMany(t => t.Members)
                .HasForeignKey(pt => pt.ProjectId)
                .OnDelete(DeleteBehavior.NoAction);
        }
    }
}
﻿using System.Linq;
using System.Threading.Tasks;
using BusinessLogic.DataTransferObjects.Project;
using BusinessLogic.Services.Projects;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProjectManager.Api.Helpers;
using ProjectManager.DataLayer.Models;

namespace ProjectManager.Api.Controllers
{
    [ApiController]
    [Route("Projects")]
    public class ProjectController : ControllerBase
    {
        private readonly IProjectService _projectService;

        public ProjectController(IProjectService projectService)
        {
            _projectService = projectService;
        }
        
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(Project), StatusCodes.Status404NotFound)]
        [Route("all/{id}")]
        public async Task<IActionResult> GetAll(string id)
        {
            var projects = await _projectService.GetAll(id);

            if (!projects.Any())
            {
                return NotFound();
            }
            
            return Ok(projects);
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ProjectDto), StatusCodes.Status404NotFound)]
        [Route("{id}")]
        public async Task<ActionResult<ProjectDto>> Get(string id)
        {
            var project = await _projectService.Get(id);

            if (project is null)
            {
                return NotFound();
            }
            
            return Ok(project);
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ProjectDetailedDto), StatusCodes.Status404NotFound)]
        [Route("{id}/details")]
        public async Task<ActionResult<ProjectDetailedDto>> GetDetailed(string id)
        {
            var project = await _projectService.GetDetailed(id);

            if (project is null)
            {
                return NotFound();
            }
            
            return Ok(project);
        }
        
        [HttpPost]
        [ValidateModel]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(Project), StatusCodes.Status200OK)]
        public async Task<IActionResult> Create(Project project)
        {
            await _projectService.Add(project);

            var returnDto = await _projectService.Get(project.Id);

            return Ok(returnDto);
        }
        
        [HttpPut("{id}")]
        [ValidateModel]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public async Task<IActionResult> Update(string id, Project project)
        {
            if (id != project.Id)
            {
                return BadRequest();
            }
            
            await _projectService.Update(project);
            
            return NoContent();
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public async Task<IActionResult> Delete(string id)
        {
            var result = await _projectService.Delete(id);

            if (!result)
            {
                return NotFound();
            }

            return NoContent();
        }
    }
}
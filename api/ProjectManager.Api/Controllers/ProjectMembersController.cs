﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BusinessLogic.DataTransferObjects.Project;
using BusinessLogic.Services.Projects;
using BusinessLogic.Services.Users;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProjectManager.Api.Helpers;
using ProjectManager.DataLayer.Models;

namespace ProjectManager.Api.Controllers
{
    [ApiController]
    [Route("Project")]
    public class ProjectMemberController : ControllerBase
    {
        private readonly IUserMemberService _userMemberService;
        private readonly IProjectService _projectService;

        public ProjectMemberController(
            IUserMemberService userMemberService, 
            IProjectService projectService)
        {
            _userMemberService = userMemberService;
            _projectService = projectService;
        }

        [HttpPost]
        [ValidateModel]
        [Route("Join")]
        public async Task<ActionResult> Join(AddMemberDto addMember)
        {
            await _projectService.AddMemberToProject(addMember);

            return Ok();
        }
        
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(IEnumerable<User>), StatusCodes.Status404NotFound)]
        [Route("{projectId}/NotJoined")]
        public async Task<ActionResult<IEnumerable<User>>> GetPotentialMembers(string projectId)
        {
            var members = await _userMemberService.GetPotentialMembers(projectId);

            if (!members.Any())
            {
                return NotFound();
            }

            return Ok(members);
        }
    }
}
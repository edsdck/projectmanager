﻿using System.Threading.Tasks;
using BusinessLogic.DataTransferObjects.Assignment;
using BusinessLogic.Services.Assignments;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProjectManager.Api.Helpers;
using ProjectManager.DataLayer.Models;

namespace ProjectManager.Api.Controllers
{
    [ApiController]
    [Route("Assignments")]
    public class AssignmentController : ControllerBase
    {
        private readonly IAssignmentService _assignmentService;

        public AssignmentController(IAssignmentService assignmentService)
        {
            _assignmentService = assignmentService;
        }

        [HttpPost]
        [ValidateModel]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(Assignment), StatusCodes.Status201Created)]
        public async Task<ActionResult<Assignment>> Create(Assignment assignment)
        {
            return await _assignmentService.Create(assignment);
        }
        
        [HttpPatch]
        [ProducesResponseType(typeof(Assignment), StatusCodes.Status200OK)]
        public async Task<ActionResult<Assignment>> UpdateStatus(AssignmentStatusDto assignmentStatusDto)
        {
            var result = await _assignmentService.UpdateAssignmentStatus(assignmentStatusDto);

            return Ok(result);
        }
    }
}
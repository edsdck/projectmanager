﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BusinessLogic.DataTransferObjects.User;
using BusinessLogic.Services.Users;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProjectManager.Api.Helpers;
using ProjectManager.DataLayer.Models;

namespace ProjectManager.Api.Controllers
{
    [ApiController]
    [Route("Users")]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<IEnumerable<User>>> GetAll()
        {
            return Ok(await _userService.GetAll());
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(User), StatusCodes.Status404NotFound)]
        [Route("{id}")]
        public async Task<ActionResult<User>> Get(string id)
        {
            var user = await _userService.Get(id);

            if (user is null)
            {
                return NotFound();
            }
            
            return Ok(user);
        }

        [HttpPost]
        [ValidateModel]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(User), StatusCodes.Status201Created)]
        public async Task<IActionResult> Create(User user)
        {
            user.Password = user.Password.ToBase64(); 
            await _userService.Add(user);
            
            return CreatedAtAction(nameof(Get), new { id = user.Id }, user);
        }

        [HttpPost]
        [Route("Login")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Login(LoginDto dto)
        {
            var user = await _userService.GetByName(dto.LoginName);

            if (user is null)
            {
                return BadRequest();
            }
            
            var requestPassword = dto.Password.ToBase64();
            if (user.Password == requestPassword)
            {
                return Ok(new {UserId = user.Id});
            }

            return BadRequest();
        }
        
        [HttpPut("{id}")]
        [ValidateModel]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public async Task<IActionResult> Update(string id, User user)
        {
            if (id != user.Id)
            {
                return BadRequest();
            }
            
            await _userService.Update(user);
            
            return NoContent();
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public async Task<IActionResult> Delete(string id)
        {
            var result = await _userService.Delete(id);

            if (!result)
            {
                return NotFound();
            }

            return NoContent();
        }
    }
}
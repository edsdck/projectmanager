import { Component, OnInit, Output, EventEmitter } from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";
import { Observable } from "rxjs";
import { UsersService } from "../services/users.service";
import { User } from "../models/user";
import { Router, ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-users",
  templateUrl: "./users.component.html",
  styleUrls: ["./users.component.css"]
})
export class UsersComponent implements OnInit {
  @Output() selectedUser = new EventEmitter<User>();
  users$: Observable<User[]>;
  userForm: FormGroup;
  loginForm: FormGroup;

  constructor(private usersService: UsersService, private router: Router) {
    this.userForm = this.createFormGroup();
    this.loginForm = new FormGroup({
      additionalName: new FormControl(""),
      password: new FormControl("")
    });
  }

  ngOnInit() {
    this.users$ = this.usersService.getUsers();
  }

  selectUser(user: User): void {
    if (user) {
      this.router.navigate(["/user", user.id]);
    }
  }

  createFormGroup() {
    return new FormGroup({
      fullName: new FormControl(""),
      additionalName: new FormControl(""),
      phone: new FormControl(""),
      password: new FormControl("")
    });
  }

  onSubmit(): void {
    let user: User = {
      fullName: this.userForm.value.fullName,
      additionalName: this.userForm.value.additionalName,
      phoneNumber: this.userForm.value.phone,
      password: this.userForm.value.password
    };

    this.usersService.createUser(user).subscribe({
      next: data => this.router.navigate(["/user", data.id])
    });
  }

  onLogin(): void {
    let login = {
      loginName: this.loginForm.value.additionalName,
      password: this.loginForm.value.password
    };

    this.usersService.loginUser(login).subscribe({
      next: result => this.router.navigate(["/user", result.userId])
    });
  }

  revert() {
    this.userForm.reset();
  }
}

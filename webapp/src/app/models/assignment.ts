export interface Assignment {
  id?: string;
  name?: string;
  description?: string;
  completed?: boolean;
  projectId?: string;
}

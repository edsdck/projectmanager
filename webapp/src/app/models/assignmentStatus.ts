export interface AssignmentStatus {
  id: string;
  completed: boolean;
}

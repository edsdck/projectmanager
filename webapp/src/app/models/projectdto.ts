export class ProjectDto {
    id: string;
    name: string;
    membersTotal: number;
}
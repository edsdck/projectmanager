export class User {
  id?: string;
  fullName?: string;
  additionalName?: string;
  phoneNumber?: string;
  password?: string;
}

import { User } from "./user";
import { Assignment } from "./assignment";

export interface Project {
  id?: string;
  name: string;
  ownerId: string;
  members?: User[];
  assignments?: Assignment[];
}

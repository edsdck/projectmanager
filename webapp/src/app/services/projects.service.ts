import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable, throwError } from "rxjs";
import { retry, catchError } from "rxjs/operators";
import { environment } from "src/environments/environment";
import { ProjectDto } from "../models/projectdto";
import { Project } from "../models/project";
import { AddMember } from "../models/addMember";

@Injectable({
  providedIn: "root"
})
export class ProjectsService {
  myAppUrl: string;
  myApiUrl: string;
  httpOptions = {
    headers: new HttpHeaders({
      "Content-Type": "application/json; charset=utf-8"
    })
  };

  constructor(private http: HttpClient) {
    this.myAppUrl = environment.appUrl;
    this.myApiUrl = "Projects/";
  }

  getProject(projectId: string): Observable<Project> {
    return this.http
      .get<Project>(this.myAppUrl + this.myApiUrl + projectId + "/details")
      .pipe(catchError(this.errorHandler));
  }

  getUserProjects(userId: string): Observable<ProjectDto[]> {
    return this.http
      .get<ProjectDto[]>(this.myAppUrl + this.myApiUrl + "all/" + userId)
      .pipe(catchError(this.errorHandler));
  }

  createProject(project: Project): Observable<Project> {
    return this.http
      .post<Project>(
        this.myAppUrl + this.myApiUrl,
        JSON.stringify(project),
        this.httpOptions
      )
      .pipe(retry(1), catchError(this.errorHandler));
  }

  addToProject(member: AddMember): Observable<AddMember> {
    return this.http
      .post<AddMember>(
        this.myAppUrl + `Project/Join`,
        JSON.stringify(member),
        this.httpOptions
      )
      .pipe(retry(1), catchError(this.errorHandler));
  }

  errorHandler(error) {
    let errorMessage = "";
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(`ERROR: ${errorMessage}`);
    return throwError(errorMessage);
  }
}

import { Injectable } from "@angular/core";
import { HttpHeaders, HttpClient } from "@angular/common/http";
import { environment } from "src/environments/environment";
import { AssignmentStatus } from "../models/assignmentStatus";
import { Observable, throwError } from "rxjs";
import { Assignment } from "../models/assignment";
import { catchError, retry } from "rxjs/operators";

@Injectable({
  providedIn: "root"
})
export class AssignmentsService {
  myAppUrl: string;
  myApiUrl: string;
  httpOptions = {
    headers: new HttpHeaders({
      "Content-Type": "application/json; charset=utf-8"
    })
  };

  constructor(private http: HttpClient) {
    this.myAppUrl = environment.appUrl;
    this.myApiUrl = "Assignments/";
  }

  create(assignment: Assignment): Observable<Assignment> {
    return this.http
      .post<Assignment>(
        this.myAppUrl + this.myApiUrl,
        JSON.stringify(assignment),
        this.httpOptions
      )
      .pipe(retry(2), catchError(this.errorHandler));
  }

  updateStatus(assignment: AssignmentStatus): Observable<Assignment> {
    return this.http
      .patch<AssignmentStatus>(
        this.myAppUrl + this.myApiUrl,
        JSON.stringify(assignment),
        this.httpOptions
      )
      .pipe(catchError(this.errorHandler));
  }

  errorHandler(error) {
    let errorMessage = "";
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(`ERROR: ${errorMessage}`);
    return throwError(errorMessage);
  }
}

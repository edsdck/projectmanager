import { Component, OnInit } from "@angular/core";
import { MatDialogRef } from "@angular/material";
import { FormGroup, FormBuilder } from "@angular/forms";

@Component({
  selector: "app-project-create-modal",
  templateUrl: "./project-create-modal.component.html",
  styleUrls: ["./project-create-modal.component.css"]
})
export class ProjectCreateModalComponent implements OnInit {
  options: FormGroup;

  constructor(
    private dialogRef: MatDialogRef<ProjectCreateModalComponent>,
    fb: FormBuilder
  ) {
    this.options = fb.group({
      projectName: ""
    });
  }

  ngOnInit() {}

  create() {
    this.dialogRef.close(this.options.value);
  }

  close() {
    this.dialogRef.close();
  }

  isNameEmpty(): boolean {
    return this.options.value.projectName.length === 0;
  }
}

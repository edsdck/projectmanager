import { Component, OnInit } from "@angular/core";
import { User } from "../models/user";
import { UsersService } from "../services/users.service";
import { ActivatedRoute, Router } from "@angular/router";
import {
  MatDialog,
  MatDialogConfig,
  MatTableDataSource
} from "@angular/material";
import { UserEditComponent } from "../user-edit/user-edit.component";
import { ProjectsService } from "../services/projects.service";
import { ProjectCreateModalComponent } from "../project-create-modal/project-create-modal.component";
import { Project } from "../models/project";

@Component({
  selector: "app-user-projects",
  templateUrl: "./user-projects.component.html",
  styleUrls: ["./user-projects.component.css"]
})
export class UserProjectsComponent implements OnInit {
  user: User;
  userId: string;
  projectsSource = new MatTableDataSource();
  displayedColumns = ["name", "membersTotal", "projectPage"];

  constructor(
    private usersService: UsersService,
    private projectsService: ProjectsService,
    private avRoute: ActivatedRoute,
    private dialog: MatDialog,
    private router: Router
  ) {
    const idParam = "id";
    if (this.avRoute.snapshot.params[idParam]) {
      this.userId = this.avRoute.snapshot.params[idParam];
    }
  }

  ngOnInit() {
    this.loadUser();
    this.projectsService.getUserProjects(this.userId).subscribe({
      next: results => (this.projectsSource.data = results)
    });
  }

  loadUser() {
    this.usersService.getUser(this.userId).subscribe({
      next: (result: User) => (this.user = result)
    });
  }

  openEditUserDialog() {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    dialogConfig.data = this.user;

    const dialogRef = this.dialog.open(UserEditComponent, dialogConfig);

    dialogRef.afterClosed().subscribe({
      next: (result: User) => {
        if (result) {
          this.user = result;
          this.usersService.updateUser(this.userId, result).subscribe();
        }
      }
    });
  }

  openCreateProjectDialog() {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    const dialogRef = this.dialog.open(
      ProjectCreateModalComponent,
      dialogConfig
    );

    dialogRef.afterClosed().subscribe({
      next: result => {
        this.createProject(result);
      }
    });
  }

  createProject(fromModal: any): void {
    if (fromModal) {
      let project: Project = {
        name: fromModal.projectName,
        ownerId: this.userId
      };

      this.projectsService.createProject(project).subscribe({
        next: res => {
          const copiedData = this.projectsSource.data.slice();
          copiedData.push(res);
          this.projectsSource.data = copiedData;
        }
      });
    }
  }

  delete() {
    this.usersService.deleteUser(this.user.id).subscribe({
      complete: () => this.router.navigate(["/"])
    });
  }
}

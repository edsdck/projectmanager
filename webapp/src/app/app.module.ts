import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { HttpClientModule } from "@angular/common/http";
import { ReactiveFormsModule } from "@angular/forms";
import { MaterialModule } from "./material-module";
import { FormsModule } from "@angular/forms";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { UsersComponent } from "./users/users.component";
import { UsersService } from "./services/users.service";
import { ProjectsService } from "./services/projects.service";
import { NoopAnimationsModule } from "@angular/platform-browser/animations";
import { UserProjectsComponent } from "./user-projects/user-projects.component";
import { UserEditComponent } from "./user-edit/user-edit.component";
import { ProjectCreateModalComponent } from "./project-create-modal/project-create-modal.component";
import { ProjectsComponent } from "./projects/projects.component";
import { AddMemberToProjectDialogComponent } from "./add-member-to-project-dialog/add-member-to-project-dialog.component";

@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    UserProjectsComponent,
    UserEditComponent,
    ProjectCreateModalComponent,
    ProjectsComponent,
    AddMemberToProjectDialogComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NoopAnimationsModule,
    HttpClientModule,
    ReactiveFormsModule,
    MaterialModule,
    FormsModule
  ],
  providers: [UsersService, ProjectsService],
  bootstrap: [AppComponent],
  entryComponents: [
    UserEditComponent,
    ProjectCreateModalComponent,
    AddMemberToProjectDialogComponent
  ]
})
export class AppModule {}

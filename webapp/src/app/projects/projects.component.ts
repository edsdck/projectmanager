import { Component, OnInit, ViewChild } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { Project } from "../models/project";
import { ProjectsService } from "../services/projects.service";
import { MatDialogConfig, MatDialog } from "@angular/material";
import { AddMemberToProjectDialogComponent } from "../add-member-to-project-dialog/add-member-to-project-dialog.component";
import { User } from "../models/user";
import { AssignmentsService } from "../services/assignments.service";
import { AssignmentStatus } from "../models/assignmentStatus";
import { Assignment } from "../models/assignment";
import { FormGroup, FormControl } from "@angular/forms";

@Component({
  selector: "app-projects",
  templateUrl: "./projects.component.html",
  styleUrls: ["./projects.component.css"]
})
export class ProjectsComponent implements OnInit {
  @ViewChild("f", { static: false }) myNgForm;
  project: Project;
  projectId: string;
  assignmentForm: FormGroup;

  constructor(
    private avRoute: ActivatedRoute,
    private projectsService: ProjectsService,
    private assignmentsService: AssignmentsService,
    private router: Router,
    private dialog: MatDialog
  ) {
    const idParam = "id";
    if (this.avRoute.snapshot.params[idParam]) {
      this.projectId = this.avRoute.snapshot.params[idParam];
    }

    this.assignmentForm = new FormGroup({
      name: new FormControl(""),
      description: new FormControl("")
    });
  }

  ngOnInit() {
    this.projectsService.getProject(this.projectId).subscribe({
      next: (result: Project) => {
        this.project = result;
        console.log(this.project);
      }
    });
  }

  changeAssignmentStatus(assignment: Assignment): void {
    let assignmentStatus: AssignmentStatus = {
      id: assignment.id,
      completed: !assignment.completed
    };

    console.log(assignmentStatus);

    this.assignmentsService.updateStatus(assignmentStatus).subscribe({
      next: ass => (assignment.completed = ass.completed)
    });
  }

  selectMember(userId: string): void {
    this.router.navigate(["/user", userId]);
  }

  openAddMemberDialog() {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = this.projectId;

    const dialogRef = this.dialog.open(
      AddMemberToProjectDialogComponent,
      dialogConfig
    );

    dialogRef.afterClosed().subscribe({
      next: result => {
        if (result) {
          let member = {
            id: result.id,
            name: result.fullName
          };

          this.project.members.push(member);
        }
      }
    });
  }

  onSubmit(): void {
    let assignment: Assignment = {
      name: this.assignmentForm.value.name,
      description: this.assignmentForm.value.description,
      projectId: this.projectId
    };

    this.assignmentsService.create(assignment).subscribe({
      next: result => {
        this.project.assignments.push(result);
        this.myNgForm.resetForm();
      }
    });
  }
}

import { Component, OnInit, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { FormControl } from "@angular/forms";
import { UsersService } from "../services/users.service";
import { User } from "../models/user";
import { Observable } from "rxjs";
import { startWith, map } from "rxjs/operators";
import { ProjectsService } from "../services/projects.service";
import { AddMember } from "../models/addMember";

@Component({
  selector: "app-add-member-to-project-dialog",
  templateUrl: "./add-member-to-project-dialog.component.html",
  styleUrls: ["./add-member-to-project-dialog.component.css"]
})
export class AddMemberToProjectDialogComponent implements OnInit {
  memberCtrl = new FormControl();
  selectedMember = null;
  users: User[];
  filteredUsers: Observable<User[]>;
  projectId: string;

  constructor(
    private dialogRef: MatDialogRef<AddMemberToProjectDialogComponent>,
    private usersService: UsersService,
    private projectsService: ProjectsService,
    @Inject(MAT_DIALOG_DATA) data: string
  ) {
    this.projectId = data;
    this.usersService.getPotentialMembers(this.projectId).subscribe({
      next: result => {
        this.users = result;
      },
      complete: () => {
        this.filteredUsers = this.memberCtrl.valueChanges.pipe(
          startWith(""),
          map(user => (user ? this._filterUsers(user) : this.users.slice()))
        );
      }
    });
  }

  ngOnInit() {}

  private _filterUsers(value: string): User[] {
    const filterValue = value.toLowerCase();

    return this.users.filter(
      user => user.fullName.toLowerCase().indexOf(filterValue) === 0
    );
  }

  memberClick(name: string) {
    this.selectedMember = this.users.find(user => user.fullName === name);
  }

  setToNullOnInput() {
    this.selectedMember = null;
  }

  addMember() {
    if (this.selectedMember) {
      let member: AddMember = {
        userId: this.selectedMember.id,
        projectId: this.projectId
      };

      this.projectsService.addToProject(member).subscribe({
        complete: () => this.dialogRef.close(this.selectedMember)
      });
    }
  }

  close(): void {
    this.dialogRef.close();
  }
}

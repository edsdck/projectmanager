import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddMemberToProjectDialogComponent } from './add-member-to-project-dialog.component';

describe('AddMemberToProjectDialogComponent', () => {
  let component: AddMemberToProjectDialogComponent;
  let fixture: ComponentFixture<AddMemberToProjectDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddMemberToProjectDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddMemberToProjectDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

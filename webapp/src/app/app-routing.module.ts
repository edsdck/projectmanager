import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { UsersComponent } from "./users/users.component";
import { UserProjectsComponent } from "./user-projects/user-projects.component";
import { ProjectsComponent } from "./projects/projects.component";

const routes: Routes = [
  { path: "", component: UsersComponent, pathMatch: "full" },
  { path: "user/:id", component: UserProjectsComponent },
  { path: "project/:id", component: ProjectsComponent },
  { path: "**", redirectTo: "/" }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
